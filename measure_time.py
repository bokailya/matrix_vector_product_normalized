from time import time

from numpy.random import rand

from matrix_vector_product_normalized import matrix_vector_product_normalized

N = 8192

input_matrix = rand(N, N)
input_vector = rand(N)

start = time()
matrix_vector_product_normalized(input_matrix, input_vector)
print(time() - start)
