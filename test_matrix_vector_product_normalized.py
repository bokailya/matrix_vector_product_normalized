"""Test matrix_vector_product_normalized module"""
from math import sqrt
from numpy import array
from numpy.testing import assert_allclose
from matrix_vector_product_normalized import matrix_vector_product_normalized


if __name__ == "__main__":
    assert_allclose(matrix_vector_product_normalized(array([
        [-sqrt(3), -sqrt(3), 0, 0],
        [0, 1, 1, 1],
        [1, 1, -1, -1],
        [-1, -1, -1, -1]]),
        array([-1, 2, 0, 1])), [-sqrt(3) / 4, 0.75, 0, -0.5])
