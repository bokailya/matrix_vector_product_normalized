"""
Module contains
function for calculating matrix vector product and normalizing it
"""
from math import sqrt
from multiprocessing import Barrier, Pipe, Process, Queue, cpu_count
from multiprocessing.shared_memory import SharedMemory
from numpy import ndarray, zeros
from numpy.linalg import norm


def matrix_vector_product_normalized(matrix, vector):
    """Calculate matrix vector product and normalize it"""
    def get_shared_array(shape, datatype, name):
        """Get shared array by attributes"""
        shared_memory = SharedMemory(name)
        return ndarray(shape, datatype, shared_memory.buf), shared_memory


    def array_to_shared(array):
        """Make shared array from any"""
        shared_memory = SharedMemory(None, True, array.nbytes)
        ndarray(array.shape, array.dtype, shared_memory.buf)[:] = array[:]
        return shared_memory


    def process_target(
            process_number,
            number_of_processes,
            m,
            n,
            matrix_datatype,
            vector_datatype,
            matrix_name,
            vector_name,
            result_pipe,
            queue,
            barrier):
        matrix, matrix_memory = get_shared_array((
            m, n), matrix_datatype, matrix_name)
        vector, vector_memory = get_shared_array((
            n,), vector_datatype, vector_name)

        values = zeros(len(range(process_number, m, number_of_processes)))
        sum_of_squares = 0
        push = 0
        for row in range(process_number, m, number_of_processes):
            values[push] = matrix[row].dot(vector)
            sum_of_squares += values[push]**2
            push += 1

        matrix_memory.close()
        vector_memory.close()

        queue.put(sum_of_squares)

        if not process_number:
            norm = sqrt(sum(queue.get() for i in range(number_of_processes)))
            matrix_memory.unlink()
            vector_memory.unlink()
            barrier.wait()
            for process_number in range(number_of_processes - 1):
                queue.put(norm)
            result_pipe.send(values / norm)
            return
        barrier.wait()
        result_pipe.send(values / queue.get())


    m = len(matrix)
    n = len(vector)

    shared_matrix = array_to_shared(matrix)
    shared_matrix.close()
    shared_vector = array_to_shared(vector)
    shared_vector.close()

    number_of_processes = cpu_count()
    result_pipes = [Pipe() for i in range(number_of_processes)]
    queue = Queue()
    barrier = Barrier(number_of_processes)
    for process in range(number_of_processes - 1):
        Process(None, process_target, None, (
            process,
            number_of_processes,
            m,
            n,
            matrix.dtype,
            vector.dtype,
            shared_matrix.name,
            shared_vector.name,
            result_pipes[process][1],
            queue,
            barrier)).start()
    process_target(
        number_of_processes - 1,
        number_of_processes,
        m,
        n,
        matrix.dtype,
        vector.dtype,
        shared_matrix.name,
        shared_vector.name,
        result_pipes[-1][1],
        queue,
        barrier)


    processes_result = [result_pipes[process][0].recv() for process in range(
        number_of_processes)]
    result = zeros(m)
    i = j = process = 0
    while i < m:
        result[i] = processes_result[process][j]
        process += 1
        if process == number_of_processes:
            j = j + 1
            process = 0
        i += 1

    return result
